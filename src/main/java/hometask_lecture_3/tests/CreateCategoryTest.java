package hometask_lecture_3.tests;

import hometask_lecture_3.BaseScript;
import hometask_lecture_3.UserActions;
import hometask_lecture_3.utils.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.Date;

public class CreateCategoryTest extends BaseScript {

    public static void main(String[] args) throws InterruptedException {
        EventFiringWebDriver driver = getConfiguredDriver();
        try {
            driver.get(Properties.getBaseAdminUrl());
            UserActions userActions = new UserActions(driver);
            userActions.loginAdminPanel();
            userActions.createCategory();
            System.out.println("<<< Script has finished successfully >>>");
            userActions.logOutAdminPanel();
        } finally {
            driver.quit();
        }
    }
}

