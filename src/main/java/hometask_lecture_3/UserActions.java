package hometask_lecture_3;

import hometask_lecture_3.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class UserActions {
    private static EventFiringWebDriver driver = null;
    private static By loginEmail = By.id("email");
    private static By password = By.id("passwd");
    private static By submitButton = By.name("submitLogin");
    private static By menuIcon = By.cssSelector(".imgm.img-thumbnail");
    private static By logoutButton = By.id("header_logout");
    private static By sideMenuItemCategory = By.xpath("//li[contains(@class,'link-levelone') or contains(@class,'maintab')][3]");
    private static By categoryTab = By.id("subtab-AdminCategories");
    private static By addNewCategoryButton = By.id("page-header-desc-category-new_category");
    private static By inputFieldCategoryName = By.id("name_1");
    private static By inputSearchFieldName = By.name("categoryFilter_name");
    private static By saveCategoryButton = By.id("category_form_submit_btn");
    private static By searchCategoryButton = By.id("submitFilterButtoncategory");
    private static By successMessage = By.cssSelector(".alert.alert-success");
    private static By searchResultField = By.xpath("//*[@class = 'pointer'][1]");
    private static By ajaxIcon = By.id("ajax_running");
    private static String categoryName = "New Category "+ dateNow();

    public UserActions(EventFiringWebDriver driver){
        this.driver = driver;
    }

    /**
     *
     * Method logins to admin panel.
     */
    public static void loginAdminPanel() {
        driver.findElement(loginEmail).sendKeys(Properties.getLogin());
        driver.findElement(password).sendKeys(Properties.getPassword());
        driver.findElement(submitButton).click();
        waitFor(sideMenuItemCategory);
    }

    /**
     * Adds new category in Admin Panel.
     *
     */
    public void createCategory() {
        waitForInvisibilityOfElement(ajaxIcon);
        Actions action = new Actions(driver);
        WebElement catalog = driver.findElement(sideMenuItemCategory);
        WebElement category = driver.findElement(categoryTab);
        action.moveToElement(catalog).build().perform();
        waitFor(categoryTab);
        action.moveToElement(category).click().build().perform();
        waitForInvisibilityOfElement(ajaxIcon);
        driver.findElement(addNewCategoryButton).click();
        driver.findElement(inputFieldCategoryName).sendKeys(categoryName);
        driver.findElement(saveCategoryButton).click();
        waitFor(successMessage);
        driver.findElement(inputSearchFieldName).sendKeys(categoryName);
        driver.findElement(searchCategoryButton).click();
        waitFor(searchResultField);
        System.out.println(driver.findElement(searchResultField).getText());
    }

    /**
     *
     * Method logouts to admin panel.
     */
    public static void logOutAdminPanel() {
        System.out.println("<<< Logging out >>>");
        driver.findElement(menuIcon).click();
        driver.findElement(logoutButton).click();
        driver.quit();
    }

    /**
     *
     * Method waits for particular time period.
     */
    public static void waitFor(By by) {
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(by));
    }

    /**
     *
     * Method waits for particular time period.
     */
    public static void waitForInvisibilityOfElement(By by) {
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.invisibilityOfElementLocated(by));
    }

    /**
     *
     * Method returns current date.
     */
    public static String dateNow () {
       return new SimpleDateFormat("yyyy-MM-dd - HH:mm:ss")
               .format(Calendar.getInstance().getTime());
    }
}